#pragma once

template <class T>
struct Node {
public:
	T Data;
	Node* Next;
	Node* Previous;

	Node() {
		Data = NULL;
		Next = nullptr;
		Previous = nullptr;
	}

	Node(T Data) {
		this->Data = Data;
		Next = nullptr;
		Previous = nullptr;
	}
};

template <class T>
class CircularLinkedList
{
public:
	CircularLinkedList();

	//Push and Pop will cout when they complete if Verbose is true
	CircularLinkedList(bool Verbose);

	~CircularLinkedList();

	// Pushed a value onto the end of the list
	void Push(T Data);

	// Pops the value of the true end of the list, returns NULL if empty
	bool Pop(T &Data);

	bool GetAtIndex(int Index, T &Data) const;

private:
	Node<T>* First;
	Node<T>* Last;

	int Length = 0;

	bool Verbose = false;
};