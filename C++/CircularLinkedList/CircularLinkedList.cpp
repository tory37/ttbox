#include "stdafx.h"
#include "CircularLinkedList.h"

template<class T>
CircularLinkedList<T>::CircularLinkedList() {
	First = nullptr;
	Last = First;
}

template<class T>
CircularLinkedList<T>::CircularLinkedList(bool Verbose) {
	First = nullptr;
	Last = First;
	this->Verbose = Verbose;
}

template<class T>
CircularLinkedList<T>::~CircularLinkedList() {
	Node<T>* Current = Last;

	if (Current == nullptr)
		return;

	First->Previous = nullptr;

	if (First == Last)
		delete First;
	else {
		do {
			Last = Current->Previous;
			delete Current;
			Current = Last;
		} while (Current != nullptr);
	}

	delete Current;
}

template<class T>
void CircularLinkedList<T>::Push(T Data) {
	if (First == nullptr)
	{
		First = new Node<T>(Data);
		Last = First;
		First->Next = First;
		First->Previous = First;
	}
	else if (First == Last)
	{
		Last = new Node<T>(Data);
		First->Next = Last;
		First->Previous = Last;

		Last->Next = First;
		Last->Previous = First;
	}
	else
	{
		Node<T>* NewNode = new Node<T>(Data);
		Last->Next = NewNode;
		NewNode->Previous = Last;
		Last = NewNode;

		Last->Next = First;
		First->Previous = Last;
	}

	Length++;

	if (Verbose)
		std::cout << "Pushed: " << Data << std::endl;
}

template<class T>
bool CircularLinkedList<T>::Pop(T &Data) {
	if (Length == 0)
	{
		if (Verbose)
			std::cout << "List is empty, nothing to pop." << std::endl;
		Data = NULL:
		return false;
	}

	Data = Last->Data;

	if (First == Last)
	{
		delete Last;
		First = nullptr;
		Last = nullptr;
	}
	else {
		Last = Last->Previous;
		delete Last->Next;
		Last->Next = First;
		First->Previous = Last;
	}

	if (Verbose)
		std::cout << "Popped: " << Data << std::endl;

	Length--;
	return true;
}

template<class T>
bool CircularLinkedList<T>::GetAtIndex(int Index, T &Data) const
{
	if (Length == 0)
	{
		if (Verbose)
			std::cout << "List is empty.";
		Data = NULL;
		return false;
	}
	if (Index >= Length)
	{
		if (Verbose)
			std::cout << "List has " << Length << "elements. Index " << Index << " doesn't exist.";
		Data = NULL;
		false;
	}

	Node<T>* Current = First;
	for (int i = 0; i < Index; i++)
		Current = Current->Next;

	Data = Current->Data;

	return true;
}
